<?php

Route::group(['prefix' => 'member', 'namespace' => 'Member'], function() {

    Route::group(['middleware' => 'auth:web'], function(){
        Route::get('', 'HomeController@index');
        Route::get('home', 'HomeController@index');

        Route::post('number/add', 'NumberController@addNumber');
    });
    
});