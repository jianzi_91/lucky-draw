<?php

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function() {

    Route::get('login', 'Auth\LoginController@getLogin');
    Route::post('login', 'Auth\LoginController@initLogin')->name('admin_login');

    Route::group(['middleware' => 'admin'], function(){
        Route::get('home', 'HomeController@index');

        Route::post('prize/update', 'PrizeController@decidePrizes');
        Route::get('prize/randomize', 'PrizeController@randomize');

        // Logout
	    Route::post('logout', 'Auth\LoginController@logout');
    });
    
    Route::get('test', 'PrizeController@randomize');
});