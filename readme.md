<h1><strong>Lucky Draw System</strong></h1>
<br>
<h3>Admin Portal</h3>
<ol>
    <li>Display all members with their registered number.</li>
    <li>Randomize grand prize number with highest number registered.</li>
    <li>Click update to save current fields as winning numbers.</li>
    <li>Backend validation enabled to ensure no duplicate numbers, no duplicate winners, a must-4-digit number.</li>
</ol>
<h3>Member Portal</h3>
<ol>
    <li>Display current member with their registered number.</li>
    <li>Able to register their lucky numbers.</li>
    <li>Backend validation enabled to ensure no duplicate numbers, a must-4-digit number.</li>
</ol>
<h3>Web</h3>
<ol>
    <li>Render results and winners' username.</li>
</ol>
<br>
<h1><strong>Guides</strong></h1>
<h3><strong>Installation</strong></h3>
<ol>
    <li>Run <code>composer update</code>.</li>
    <li>Import database from LuckyDraw.sql from database folder at root. *<i><small>The sql will generate schema for you as well.</small></i></li>
    <li>Create .env file from .env.example and configure the database accordingly.</li>
    <li>Ta-da!</li>
</ol>

<h3><strong>Extras</strong></h3>
<ul>
    <li>Javascript/jQuery</li>
    <li>CSS / Bootstrap 4.3.1 </li>
    <li>animate.css (<a href="https://github.com/daneden/animate.css">https://github.com/daneden/animate.css</a>)</li>
</ul>

<h3><strong>Efforts</strong></h3>
<ul>
    <li>Routes are guarded by different guards and are in separate file for better management.</li>
    <li>Eloquent <strong>Relationship</strong> are used for better data controls.</li>
    <li>Backend Validations</li>
    <li>Ajax for number randomization.</li>
    <li>With passion. &#10084;</li>
</ul>
