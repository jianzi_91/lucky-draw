CREATE DATABASE  IF NOT EXISTS `luckydraw` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `luckydraw`;
-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: luckydraw
-- ------------------------------------------------------
-- Server version	5.7.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ld_admins`
--

DROP TABLE IF EXISTS `ld_admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ld_admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ld_admins`
--

LOCK TABLES `ld_admins` WRITE;
/*!40000 ALTER TABLE `ld_admins` DISABLE KEYS */;
INSERT INTO `ld_admins` VALUES (1,'admin','$2a$10$t.9VHTYOPqq4JocAywOmTOVBfy.tdL.CQUMFUZlLPkTQlHUM8WjjK',NULL,'2019-05-13 16:00:00','2019-05-13 16:00:00');
/*!40000 ALTER TABLE `ld_admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ld_members`
--

DROP TABLE IF EXISTS `ld_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ld_members` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ld_members`
--

LOCK TABLES `ld_members` WRITE;
/*!40000 ALTER TABLE `ld_members` DISABLE KEYS */;
/*!40000 ALTER TABLE `ld_members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ld_numbers`
--

DROP TABLE IF EXISTS `ld_numbers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ld_numbers` (
  `number` varchar(4) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  UNIQUE KEY `number_UNIQUE` (`number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ld_numbers`
--

LOCK TABLES `ld_numbers` WRITE;
/*!40000 ALTER TABLE `ld_numbers` DISABLE KEYS */;
/*!40000 ALTER TABLE `ld_numbers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ld_prizes`
--

DROP TABLE IF EXISTS `ld_prizes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ld_prizes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `grand` varchar(4) NOT NULL,
  `second_one` varchar(4) NOT NULL,
  `second_two` varchar(4) NOT NULL,
  `third_one` varchar(4) NOT NULL,
  `third_two` varchar(4) NOT NULL,
  `third_three` varchar(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ld_prizes`
--

LOCK TABLES `ld_prizes` WRITE;
/*!40000 ALTER TABLE `ld_prizes` DISABLE KEYS */;
/*!40000 ALTER TABLE `ld_prizes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'luckydraw'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-16  3:11:39
