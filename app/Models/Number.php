<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Number extends Model
{
    protected $table = "numbers";
    protected $guarded = [];
    public $timestamps = false;

    public function member()
    {
        return $this->belongsTo(Member::class, 'member_id', 'id');
    }
}
