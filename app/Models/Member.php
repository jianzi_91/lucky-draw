<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Member extends Authenticatable
{
    use Notifiable;

    protected $table = "members";
    protected $guarded = [];

    public function numbers()
    {
        return $this->hasMany(Number::class, 'member_id', 'id');
    }
}
