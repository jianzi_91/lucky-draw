<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Prize extends Model
{
    protected $table = "prizes";
    protected $guarded = [];

    public function grandPrize()
    {
        return $this->hasOne(Number::class, 'number', 'grand');
    }

    public function secondOne()
    {
        return $this->hasOne(Number::class, 'number', 'second_one');
    }

    public function secondTwo()
    {
        return $this->hasOne(Number::class, 'number', 'second_two');
    }

    public function thirdOne()
    {
        return $this->hasOne(Number::class, 'number', 'third_one');
    }

    public function thirdTwo()
    {
        return $this->hasOne(Number::class, 'number', 'third_two');
    }

    public function thirdThree()
    {
        return $this->hasOne(Number::class, 'number', 'third_three');
    }

    public function getGrandWinnerAttribute()
    {
        return isset($this->grandPrize->member)?'('.$this->grandPrize->member->username.')':null;
    }

    public function getSecondOneWinnerAttribute()
    {
        return isset($this->secondOne->member)?'('.$this->secondOne->member->username.')':null;
    }

    public function getSecondTwoWinnerAttribute()
    {
        return isset($this->secondTwo->member)?'('.$this->secondTwo->member->username.')':null;
    }

    public function getThirdOneWinnerAttribute()
    {
        return isset($this->thirdOne->member)?'('.$this->thirdOne->member->username.')':null;
    }

    public function getThirdTwoWinnerAttribute()
    {
        return isset($this->thirdTwo->member)?'('.$this->thirdTwo->member->username.')':null;
    }

    public function getThirdThreeWinnerAttribute()
    {
        return isset($this->thirdThree->member)?'('.$this->thirdThree->member->username.')':null;
    }
}
