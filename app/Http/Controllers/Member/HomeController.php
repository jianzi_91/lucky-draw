<?php

namespace App\Http\Controllers\Member;

use Auth;

use App\Models\Member;
use App\Models\Number;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $member = Auth::user();
        $numbers = $member->numbers;

        return view('member.home', compact('numbers'));
    }
}
