<?php

namespace App\Http\Controllers\Member;

use Auth;
use Request;
use Validator;

use App\Models\Member;
use App\Models\Number;

class NumberController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function addNumber()
    {
        $data = Request::only('number');

        $v = Validator::make($data, [
            'number' => 'required|numeric|unique:numbers,number|digits:4',
        ]);

        if ($v->fails()) {

            return back()->withInput()->withErrors($v);
        }

        $number = Number::create([
            'number' => $data['number'],
            'member_id' => Auth::user()->id,
        ]);

        return back()->with('success', 'You just registered ' . $data['number'] . ' as your lucky number.');
    }
}
