<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Request;
use Validator;

use App\Models\Member;
use App\Models\Number;
use App\Models\Prize;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $members = Member::all();
        $prizes = Prize::orderBy('created_at', 'desc')->first();

        return view('admin.home', compact('members','prizes'));
    }
}
