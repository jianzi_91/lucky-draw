<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Request;
use Response;
use Validator;

use App\Models\Member;
use App\Models\Number;
use App\Models\Prize;

class PrizeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function randomize()
    {
        $pool = $this->generatePool();

        $grand = array_first($this->randomPick($pool['grand'], 1));

        if (!$grand) {
            $grand = $this->generateRandomNumber();
        }
        
        // Return the rest of the numbers in pool after picked for grand
        $members = Member::has('numbers', $pool['highscore'])->get(); // get members who has the highest number

        if ($members->count() > 1) {

            $grand_number = Number::where('number', $grand)->first();
            $grand_number_owner = $grand_number->member;

            foreach ($members as $member) {
                
                if ($member->id == $grand_number_owner->id) {
                    continue;
                }

                foreach ($member->numbers as $number) {
                    array_push($pool['prize'], $number->number); // Put back to pool
                }
            }
        }
        // End of returning

        $prizes = [];

        for ($i = 0; $i < 5; $i++) {

            // In case the member less than 6, fills the slots left with random 4 digits number
            if (empty($pool['prize'])) {
                $prizes[$i] = $this->generateRandomNumber();
                continue;
            }

            $prizes[$i] = array_first($this->randomPick($pool['prize'], 1));

            $prize = Number::where('number', $prizes[$i])->first();
            $prize_owner = $prize->member;
            $owner_lucky_numbers = $this->getMemberLuckyNumbers($prize_owner->id);

            $pool['prize'] = array_values(array_diff($pool['prize'], $owner_lucky_numbers));

        }

        return Response::json([
            'grand' => $grand,
            's1' => $prizes[0],
            's2' => $prizes[1],
            't1' => $prizes[2],
            't2' => $prizes[3],
            't3' => $prizes[4],
        ]);

    }

    protected function generatePool()
    {
        $members = Member::all();
        $grand_pool = []; // Initiate empty array to store numbers for grand prize pool
        $prize_pool = []; // Same goes to normal prize pool for second and third
        $highscore_now = 0;

        foreach($members as $member) {

            $cmp = strcmp($member->numbers()->count(), $highscore_now); // Compare the member's number count

            switch (true) {
                case ($cmp < 0): // Nothing need to be happen if less than highscore

                    break;
                case ($cmp == 0): // When it is same
                    
                    //Put them into existing grand prize pool 
                    if ($highscore_now == $member->numbers()->count()) {
                        foreach ($member->numbers as $number) {
                            array_push($grand_pool, $number->number);
                        }
                    }

                    break;
                case ($cmp > 0): // When a new highscore found

                    $grand_pool = []; // Empty out current grand prize pool
                    $highscore_now = $member->numbers()->count(); // Set current highest count

                    // Insertion
                    foreach ($member->numbers as $number) {
                        array_push($grand_pool, $number->number);
                    }

                    break;
                default:
                    break;
            }

            // Insert all lucky number into prize pool
            foreach ($member->numbers as $number) {
                array_push($prize_pool, $number->number);
            }
            
        }

        // Filter out numbers inside grand_pool
        $prize_pool = array_values(array_diff($prize_pool, $grand_pool));
        
        return array('grand' => $grand_pool, 'prize' => $prize_pool, 'highscore' => $highscore_now);
    }

    protected function randomPick($pool, $quantity) {

        shuffle($pool);

        return array_slice($pool, 0, $quantity);
    }

    protected function getMemberLuckyNumbers($member_id)
    {
        $member = Member::where('id', $member_id)->first();
        
        return $member->numbers->pluck('number')->toArray();

    }

    protected function generateRandomNumber()
    {
        return str_pad(mt_rand(0, 9999), 4, "0", STR_PAD_LEFT);
    }

    public function decidePrizes()
    {
        $data = Request::only('grand','s1','s2','t1','t2','t3');

        $v = Validator::make($data, [
            'grand' => 'required|digits:4',
            's1' => 'required|digits:4',
            's2' => 'required|digits:4',
            't1' => 'required|digits:4',
            't2' => 'required|digits:4',
            't3' => 'required|digits:4',
        ]);

        if ($v->fails()) {
            
            return back()->withInput()->withErrors($v);
        }

        $v->after(function($v) use ($data){

            $lucky_numbers = array_values($data);

            if (count($lucky_numbers) !== count(array_unique($lucky_numbers))) {
                
                $v->errors()->add('errors', 'There are duplicated number!');
            }

            $winners = [];

            foreach($lucky_numbers as $lucky_number) {
                
                $number = Number::where('number', $lucky_number)->first();

                if (isset($number) && $number->has('member')) {
                    array_push($winners, $number->member->id);
                }

            }

            if (count($winners) != count(array_unique($winners))) {
                
                $v->errors()->add('errors', 'There are duplicated winner.');
            }

        });

        if ($v->fails()) {
            
            return back()->withInput()->withErrors($v);
        }

        Prize::create([
            'grand' => $data['grand'],
            'second_one' => $data['s1'],
            'second_two' => $data['s2'],
            'third_one' => $data['t1'],
            'third_two' => $data['t2'],
            'third_three' => $data['t3'],
        ]);

        return back()->with('success', 'Prizes are set!');
    }

}
