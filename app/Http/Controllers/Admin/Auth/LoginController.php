<?php

namespace App\Http\Controllers\Admin\Auth;

use Auth;
use Validator;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;

use App\Http\Controllers\Admin\Controller;
use App\Models\Admin;

class LoginController extends Controller 
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo;
    protected $guard = 'admins';
    protected $username = 'username';
    protected $maxAttempts = 3; // Amount of bad attempts user can make
    protected $decayMinutes = 15; // Time for which user is going to be blocked in minutes

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->redirectAfterLogout = 'admin/login';
        $this->redirectTo = 'admin/home';
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'username';
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard($this->guard);
    }

    public function getLogin() 
    {
        if ($this->guard()->check()) {
            return redirect($this->redirectTo);
        }

        return view('admin.auth.login');
    }

    public function showRegistrationForm() 
    {
        return view('admin.auth.register');
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     * @see Illuminate\Foundation\Auth\AuthenticatesUsers@validateLogin
     */
    protected function validateLogin(Request $request)
    {
        $niceNames = [
             $this->username() => trans('field.admin.username'),
            'password' => trans('field.admin.password'),
        ];

       // Session::put('captcha_code', 'somthing');
        $v = Validator::make($request->all(), [
            $this->username() => 'required|string', 
            'password' => 'required|string', 
        ], [], $niceNames);

        if ($v->fails()) {
            throw ValidationException::withMessages(
                $v->errors()->toArray()
            );
        }
    }

    /**
     * Handle a init login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function initLogin(Request $request)
    {
        //remove remember me
        if ($request->has("remember")) {
            $request->request->remove("remember");
        }
        
        return $this->login($request);
    }

    public function authenticated(Request $request, Admin $user)
    {
        //if user is authenticated
        $v = Validator::make($request->all(), [
            $this->username() => 'required', 
        ]);

        if ($v->fails()) {
            $this->guard()->logout();
            throw ValidationException::withMessages(
                $v->errors()->toArray()
            );
        }

        //dd($this->redirectPath());
        return redirect()->to($this->redirectPath());
        // return redirect()->intended($this->redirectPath()); //need to do study
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect($this->redirectAfterLogout);
    }
}
