<div class="row">
    <div class="col-md-12">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                <h4>Success!</h4>
                {{ session('success') }}
            </div>
        @endif

        @if (session('info'))
            <div class="alert alert-info" role="alert">
                <h4>Attention!</h4>
                {!! session('info') !!}
            </div>
        @endif

        @if (session('warning'))
            <div class="alert alert-warning" role="alert">
                <h4>Warning!</h4>
                {!! session('warning') !!}
            </div>
        @endif

        @if (!empty($errors->all()))
            <div class="alert alert-danger" role="alert">
                <h4>Error!</h4>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{!! $error !!}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>