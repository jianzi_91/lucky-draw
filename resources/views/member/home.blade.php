@extends('layouts.member_master')

@section('content')

<div class="row" style="padding-left: 5%; padding-right: 5%; padding-top: 5%;">

    <div class="col-md-12 text-center">
        <h3>Username: {{ Auth::user()->username }}</h3>
    </div>
    <hr>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header text-center">
                <h2>My lucky numbers</h2>
            </div>
            <div class="card-body">
                @if(count($numbers) == 0)
                    <p>You do not have any number yet.</p>
                @else
                    <ol>
                    @foreach ($numbers as $number)
                        <li>{{ $number->number }}</li>
                    @endforeach
                    </ol>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <form action="/member/number/add" method="POST">
                @csrf
            <div class="card-header text-center">
                <h2>Enter your lucky number</h2>
            </div>
            <div class="card-body">
                @include('partials.notification')
                <div class="form-group">
                    <input class="form-control" name="number" maxlength="4" minlength="4" autocomplete="off">
                </div>
            </div>
            <div class="card-footer text-center">
                <button type="submit" class="btn btn-primary">Add</button>
            </div>
            </form>
        </div>
    </div>
</div>

@endsection