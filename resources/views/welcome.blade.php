@extends('layouts.app')

@section('content')

<div class="row" style="margin-top: 10%;">
    <div class="col"></div>
    <div class="col-6">
        @if($prizes)
        <table class="table table-bordered table-hover" style="border: 2px solid black;">
            <thead>
                <tr>
                    <th style="border: 2px solid black;">Prize</th>
                    <th colspan="6" style="border: 2px solid black;">Winning Numbers</th>
                </tr>
            </thead>
            <tbody>
                <tr style="background-color: gold;">
                    <th style="border: 2px solid black;">Grand</th>
                    <td colspan="6" class="text-center">{{ $prizes->grand }} {{ $prizes->grandWinner }}</td>
                </tr>
                <tr style="background-color: silver;">
                    <th style="border: 2px solid black;">Second</th>
                    <td colspan="3" class="text-center">
                        {{ $prizes->second_one }} {{ $prizes->secondOneWinner }}
                    </td>
                    <td colspan="3" class="text-center">
                        {{ $prizes->second_two }} {{ $prizes->secondTwoWinner }}
                    </td>
                </tr>
                <tr style="background-color: chocolate;">
                    <th style="border: 2px solid black;">Third</th>
                    <td colspan="2" class="text-center">
                        {{ $prizes->third_one }} {{ $prizes->thirdOneWinner }}
                    </td>
                    <td colspan="2" class="text-center">
                        {{ $prizes->third_three }} {{ $prizes->thirdTwoWinner }}
                    </td>
                    <td colspan="2" class="text-center">
                        {{ $prizes->third_three }} {{ $prizes->thirdThreeWinner }}
                    </td>
                </tr>
            </tbody>
        </table>
        @else
        <div class="jumbotron animated infinite pulse" style="opacity: 0.75;">
            <h1 class="text-center">This is a lucky draw system.</h1>
            <br>
            <h2 class="text-center">There is no winning numbers announced yet. Please proceed to login or register to start using this system.</h2>
        </div> 
        @endif
    </div>
    <div class="col"></div>
</div>

@endsection