@extends('layouts.admin_master')

@section('content')

<div class="row" style="padding-top: 5%;">
    <div class="col"></div>
    <div class="col-6">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th class="text-center">Username</th>
                    <th class="text-center">Numbers</th>
                </tr>
            </thead>
            <tbody>
                @foreach($members as $member)
                <tr>
                    <td>{{ $member->username }}</td>
                    <td>
                        @foreach($member->numbers as $number)
                        <span>{{ $number->number }}</span>
                        @endforeach
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="col"></div>
</div>

@endsection