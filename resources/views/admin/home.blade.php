@extends('layouts.admin_master')

@section('content')



<div class="row" style="padding-top: 5%;">
    <div class="col"></div>
    <div class="col-6">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th class="text-center">Username</th>
                    <th class="text-center">Numbers</th>
                </tr>
            </thead>
            <tbody>
                @foreach($members as $member)
                <tr>
                    <td>{{ $member->username }}</td>
                    <td>
                        @foreach($member->numbers as $number)
                        <span>{{ $number->number }}</span>
                        @endforeach
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="col"></div>
</div>

<div class="row">
    <div class="col"></div>
    <div class="col-6">
        @include('partials.notification')
    </div>
    <div class="col"></div>
</div>
<div class="row" style="padding-top: 5%;">
    <div class="col"></div>
    <div class="col-6">
        <table class="table table-bordered">
            <form action="/admin/prize/update" method="POST">
                @csrf
            <thead>
                <tr>
                    <th class="text-center">Prize</th>
                    <th class="text-center" colspan="6">Winning Number</th>
                </tr>
            </thead>
            <tbody>
                
                <tr>
                    <td>Grand</td>
                    <td colspan="6"><input id="grand" class="form-control" name="grand" style="text-align: center;" minlength="4" maxlength="4" value="{{ isset($prizes->grand)?$prizes->grand:old('grand') }}" autocomplete="off"></td>
                </tr>
                <tr>
                    <td>Second</td>
                    <td colspan="3">
                        <input id="s1" class="form-control" name="s1" style="text-align: center;" minlength="4" maxlength="4" value="{{ isset($prizes->second_one)?$prizes->second_one:old('s1') }}" autocomplete="off">
                    </td>
                    <td colspan="3">
                        <input id="s2" class="form-control" name="s2" style="text-align: center;" minlength="4" maxlength="4" value="{{ isset($prizes->second_two)?$prizes->second_two:old('s2') }}" autocomplete="off">
                    </td>
                </tr>
                <tr>
                    <td>Third</td>
                    <td colspan="2">
                        <input id="t1" class="form-control" name="t1" style="text-align: center;" minlength="4" maxlength="4" value="{{ isset($prizes->third_one)?$prizes->third_one:old('t1') }}" autocomplete="off">
                    </td>
                    <td colspan="2">
                        <input id="t2" class="form-control" name="t2" style="text-align: center;" minlength="4" maxlength="4" value="{{ isset($prizes->third_two)?$prizes->third_two:old('t2') }}" autocomplete="off">
                    </td>
                    <td colspan="2">
                        <input id="t3" class="form-control" name="t3" style="text-align: center;" minlength="4" maxlength="4" value="{{ isset($prizes->third_three)?$prizes->third_three:old('t3') }}" autocomplete="off">
                    </td>
                </tr>
            </tbody>
            <tfoot class="text-center">
                <tr>
                    <td colspan="7">
                        <button id="randomize" type="button" class="btn btn-outline-primary">Randomize</button>
                        <button class="btn btn-primary">Update</button>
                    </td>
                </tr>
            </tfoot>
            </form>
        </table>
    </div>
    <div class="col"></div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function(){

        $('#randomize').click(function() {
            $.get('/admin/prize/randomize').done(function(data){
                $('#grand').val(data.grand);
                $('#s1').val(data.s1);
                $('#s2').val(data.s2);
                $('#t1').val(data.t1);
                $('#t2').val(data.t2);
                $('#t3').val(data.t3);
            });
        });

    });
</script>
@endsection